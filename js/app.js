// Variabels
//Access to the card buddis
const cardBody = document.querySelector('#card-body');
const cartList = document.querySelector('#cartTable tbody')

// EventListeners
eventListeners()

function eventListeners() {
    // Add the course to cart
    cardBody.addEventListener('click', addToCart);
    // Load the courses when page loaded
    document.addEventListener('DOMContentLoaded', loadCourses);
    // Remove the course from cart list
    cartList.addEventListener('click', removeFromCart)
}
// Add the course to cart list
function addToCart(e) {
    let course;
    // Check the target
    if (e.target.classList.contains('btn-success')) {
        // Access to Course Body
        course = e.target.parentElement.parentElement;
        // Add the course to list
        addToList(course)
    }
}
// add the course to cart body
function addToList(courseBody) {
    // get the require information from DOM
    const courseObj = {
        courseImage: courseBody.querySelector('img').src,
        courseName: courseBody.querySelector('h5').textContent,
        coursePrice: courseBody.querySelector('#coursePrice').textContent
    }
    // Create tr tag
    const tr = document.createElement('tr');
    // Edit the tr tag
    tr.innerHTML = `
        <tr>
            <td><img src='${courseObj.courseImage}' /></td>
            <td>${courseObj.courseName}</td>
            <td>${courseObj.coursePrice}</td>
            <td><button type="button" class="btn btn-danger">X</button></td>
        </tr>
    `
    // Append the tr tag
    cartList.appendChild(tr);
    // Add the course details to localStorage
    addToLS(courseObj);
}
// Add the course details to localStorage
function addToLS(courseObj) {
    // Access to the courses from LocalStorage
    let coursesFromLS = getCourseFromLS();
    // push the course to localStorage
    coursesFromLS.push(courseObj)
    localStorage.setItem('courses', JSON.stringify(coursesFromLS))

}
// get courses from localStorage
function getCourseFromLS() {
    // Access to the localStorage
    const courseLS = localStorage.getItem('courses');
    let courses;
    // Check the courseLS
    if (!courseLS) {
        courses = [];
    } else {
        courses = JSON.parse(courseLS);
    }
    return courses;
}
// Load the courses when page loaded
function loadCourses() {
    let coursesFromLS = getCourseFromLS();
    coursesFromLS.map(courseObj => {
        // Create tr tag
        const tr = document.createElement('tr');
        // Edit the tr tag
        tr.innerHTML = `
        <tr>
            <td><img src='${courseObj.courseImage}' /></td>
            <td>${courseObj.courseName}</td>
            <td>${courseObj.coursePrice}</td>
            <td><button type="button" class="btn btn-danger">X</button></td>
        </tr>
    `
        // Append the tr tag
        cartList.appendChild(tr);
    })
}
// Remove the course from cart
function removeFromCart(e){
    if(e.target.classList.contains('btn-danger')){
        e.target.parentElement.parentElement.remove()
    }

}